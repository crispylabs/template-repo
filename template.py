"""
TEMPLATE.PY
v0

##### AUTHOR #####
Dean Pierozzi

## CONTRIBUTORS ##
-

## DEPENDENCIES ##
numpy

#### PROJECT #####
TemplateProject

## DESCRIPTION ###
Small stub to provide templates and practices for every personal .py to implement

##### NOTES ######
@self - produce more templates for dbmanagement & data processing stubs


"""

import numpy as np

print(np.array([2,3,45,5]))